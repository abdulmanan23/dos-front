import {
  FETCH_OFFERS,
  FETCH_MERCHANTS,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  FETCH_DETAILS,
} from "../redux/action-types";

export function fetchOffers(data) {
  return {
    type: FETCH_OFFERS,
    payload: data.data.payload.data,
  };
}
export function fetchDetails(data) {
  console.log("payload=", data);
  return {
    type: FETCH_DETAILS,
    payload: data,
  };
}
export function fetchMerchants(data) {
  console.log("Merchant in action=", data);
  return {
    type: FETCH_MERCHANTS,
    payload: data.data.payload.data,
  };
}
export function registerSuccess(data) {
  console.log("Register user credentials in action=", data);
  return {
    type: LOGIN_SUCCESS,
    payload: data.data.payload.data,
  };
}
export function loginSuccess(data) {
  console.log("Login credentials in action=", data);
  return {
    type: REGISTER_SUCCESS,
    payload: data.data.payload.data,
  };
}
