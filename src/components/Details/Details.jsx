import React, { Component } from "react";
import "./Details.css";
import GameInstructions from "../GameInstructions/GameInstructions";
import DetailsSidebar from "../DetailsSidebar/DetailsSidebar";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import get from "lodash/get";
import Modal from "../UI/Modal/Modal";
import {
  MDBContainer,
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
} from "mdbreact";
class Product extends Component {
  componentWillMount(props) {}
  state = {
    modal: false,
    showModal: false,
  };
  toggleModal = () => {
    this.setState({ modal: !this.state.modal });
  };
  render() {
    const { offerDetails } = this.props;

    return (
      <React.Fragment>
        <div className=" container-fluid margin">
          <div className="row">
            <div className="col-lg-3" key={offerDetails._id}>
              <img
                src={offerDetails.thumb}
                alt="pic1"
                className="ml-auto img2-style"
              />
            </div>
            <div className="col-lg-6 details-col" key={offerDetails.offerId}>
              <div className="col-lg-12 d-flex">
                <div className="col-lg-3 ">
                  <img
                    src={offerDetails.thumb}
                    alt="pic1"
                    className="img-style "
                  />
                </div>
                <div className="col-lg-5 ml-auto ">
                  <p>
                    Product: <b>{offerDetails.title}</b>
                  </p>
                  <p>
                    Merchant:<b>{offerDetails.added_by}</b>
                  </p>
                  <p>
                    Validity: <b>Valid for 3 users</b>
                  </p>
                  <p>
                    Game Mode:<b>{offerDetails.game_mode}</b>
                  </p>
                </div>
                <div className="col-lg-4 ml-auto ">
                  {/* <Link to="/login"> */}
                  <button
                    className="btn btn-info btn-circle"
                    onClick={this.toggleModal}
                  >
                    Play Now{" "}
                  </button>
                  {/* </Link> */}
                </div>
              </div>
              <GameInstructions />
            </div>
            <div className="col-lg-3 border">
              <h5>{offerDetails.title}</h5>
              <DetailsSidebar />
              <Modal />
            </div>
          </div>
          <hr></hr>
        </div>
        <MDBContainer>
          <MDBModal
            isOpen={this.state.modal}
            toggle={this.toggleModal}
            size="fluid"
          >
            <MDBModalHeader toggle={this.toggleModal}>
              Game Play Instructions
            </MDBModalHeader>
            <MDBModalBody>
              <ol>
                <li>
                  <p className="instructions">
                    <b>Click The Spin button to spin wheel.</b>
                  </p>
                </li>
                <li>
                  <p className="instructions">
                    <b>There will be different sections inside a circle.</b>
                  </p>
                </li>
                <li>
                  <p className="instructions">
                    {" "}
                    <b>
                      Stop your wheel Price section right beneath the arrow
                      head.
                    </b>{" "}
                  </p>
                </li>
                <li>
                  <p className="instructions">
                    <b>Click Stop Button during rotation to stop it.</b>
                  </p>
                </li>
              </ol>
            </MDBModalBody>
            <MDBModalFooter>
              <Link to="/login">
                <MDBBtn color="success">Play Game</MDBBtn>
              </Link>
              <MDBBtn color="danger" onClick={this.toggleModal}>
                Cancel
              </MDBBtn>
            </MDBModalFooter>
          </MDBModal>
        </MDBContainer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("state=", state.offerDetails);
  return {
    offers: get(state, "availableOffers"),
    offerDetails: state.offerDetails,
  };
};

export default connect(mapStateToProps, null)(Product);
