import React, { Component } from "react";
export class GameInstructions extends Component {
  render() {
    return (
      <div>
        <hr></hr>
        <div className="padd">
          <h5>Description</h5>
          <p>
            <b>
              The offer comprises of 20% off on this product.In order to avail
              this offer you have to first win the game.You play game by
              clicking play now button.
            </b>
          </p>
          <h5 className="mt-5">Game Play Instructions</h5>

          <ol>
            <li>
              <p className="instructions">
                <b>Click The Spin button to spin wheel.</b>
              </p>
            </li>
            <li>
              <p className="instructions">
                <b>There will be different sections inside a circle.</b>
              </p>
            </li>
            <li>
              <p className="instructions">
                {" "}
                <b>
                  Stop your wheel Price section right beneath the arrow head.
                </b>{" "}
              </p>
            </li>
            <li>
              <p className="instructions">
                <b>Click Stop Button during rotation to stop it.</b>
              </p>
            </li>
          </ol>
          <hr></hr>
          <h5>What's New</h5>
          <p>
            Bumper 1.2.0 is a major update that brings many request features:
          </p>
          <ul>
            <li>
              Custom browser rules always open up specific domains with specific
              browsers
            </li>
          </ul>
          <hr></hr>
        </div>
      </div>
    );
  }
}

export default GameInstructions;
