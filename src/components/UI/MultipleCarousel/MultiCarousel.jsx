import React from "react";
import { MDBCarousel, MDBCarouselInner, MDBLink , MDBCarouselItem, MDBRow, MDBCol } from "mdbreact";
import toy from '../../../assets/items/toy.png';
import headphone from '../../../assets/items/headphone.png';
import iphone from '../../../assets/items/iphone.png';
import shoes from '../../../assets/items/shoes.png';
import lipstick from '../../../assets/items/lipstick.png';
import camera from '../../../assets/items/camera.png';
import gamepad from '../../../assets/items/gamepad.png';
import sofa from '../../../assets/items/sofa.png';
import ring from '../../../assets/items/ring.png';
import smartwatch from '../../../assets/items/smartwatch.png';
import macbook from '../../../assets/items/macbook.png';
import speaker from '../../../assets/items/Speaker.png'
import hammer from '../../../assets/items/hammer.png';
import air_conditioner from '../../../assets/items/air_conditioner.png';
import glasses from '../../../assets/items/glasses.png';
import t_shirts from '../../../assets/items/t_shirt.png';
import calculator from '../../../assets/items/calculator.png';
import feeder from '../../../assets/items/feeder.png';
import {UNITY} from '../../../constants/Constants';
import {YAHOO} from '../../../constants/Constants';
import {AMAZON} from '../../../constants/Constants';
import {NIKEE} from '../../../constants/Constants';
import {SAMSUNG} from '../../../constants/Constants';
import './MultipleCarousel.css';


const MultiCarouselPage = () => {
  return (
 
    // <MDBContainer className="mt-1">
      <MDBCarousel activeItem={1} length={4} slide={true}  showControls={false} showIndicators={true}  multiItem>
        <MDBCarouselInner >
            <MDBCarouselItem itemId="1" >
              <MDBRow >
              <MDBCol md="2" className="col2-style" >
                  <img className="img-fluid mac-style" src={macbook} 
                    alt="img4" />
                    <MDBLink to="/">
                        MacBook
                    </MDBLink>
                </MDBCol> 
                
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid img1-section" src={headphone} 
                  style={{height:'75px' , width:'85px'}} alt="img2" />
                 
                    <MDBLink to='/'>
                        Headphone
                    </MDBLink>
                  
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid  image-style" src={camera} 
                   alt="img3" />
                    <MDBLink to='/'>
                        Web Camera 
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid image-style" src={gamepad} 
                   alt="img4" />
                    <MDBLink to='/'>
                        GamePad 
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2"  className="col2-style">
                    <img className="img-fluid image-style " src={iphone} 
                     alt="img1" />
                      <MDBLink to='/merchant-details'>
                          SAMSUNG
                      </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid  image-style" src={speaker} 
                   alt="img3" />
                    <MDBLink to='/'>
                       Wireless Speaker 
                    </MDBLink>
                </MDBCol>
              </MDBRow>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="2">
            <MDBRow>
                <MDBCol md="2 " className="col2-style">
                    <img className="img-fluid image-style" src={lipstick} 
                     alt="img1" />
                      <MDBLink to='/'>
                          Cosmetics
                      </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid image-style" src={ring} 
                 alt="img2" />
                    <MDBLink to='/'>
                        Jewelery
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid image-style" src={shoes} 
                   alt="img3" />
                    <MDBLink to='/'>
                        Fashion
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid image-style" src={smartwatch} 
                   alt="img4" />
                    <MDBLink to='/'>
                        Smart Watches
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid glass-style" src={glasses} 
                   alt="img4" />
                    <MDBLink to='/'>
                        Glasses
                    </MDBLink>
                </MDBCol> 
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid glass-style" src={t_shirts} 
                   alt="img4" />
                    <MDBLink to='/'>
                        Glasses
                    </MDBLink>
                </MDBCol>           
              </MDBRow>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="3">
              <MDBRow>
                <MDBCol md="2 " className="col2-style">
                    <img className="img-fluid toy-style" src={toy} 
                     alt="img1" />
                      <MDBLink to='/'>
                         Toys
                      </MDBLink>
                </MDBCol>
                <MDBCol md="2 " className="col2-style">
                    <img className="img-fluid image-style" src={feeder} 
                     alt="img1" />
                      <MDBLink to='/'>
                        Baby
                      </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid image-style" src={air_conditioner} 
                   alt="img2" />
                    <MDBLink to='/'>
                       Electronic Devices
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="img-fluid " src={sofa} 
                  style={{height:'75px' , width:'135px'}} alt="img3" />
                    <MDBLink to='/'>
                        Furniture
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className=" image-style" src={hammer} 
                  alt="img4" />
                    <MDBLink to='/'>
                        Tools
                    </MDBLink>
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className=" image-style" src={calculator} 
                  alt="img4" />
                    <MDBLink to='/'>
                        Calculator
                    </MDBLink>
                </MDBCol>
              </MDBRow>
            </MDBCarouselItem> 
            <MDBCarouselItem itemId="4">
            <MDBRow>
                <MDBCol md="2 " className="col2-style">
                    <img className="brand-style" src={UNITY} 
                      alt="img1" />
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="brand-style" src={YAHOO}
                   alt="img2" /> 
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="brand-style" src={AMAZON} 
                    alt="img3" />
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="brand-style" src={NIKEE}
                   alt="img4" />
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="brand-style" src={SAMSUNG}
                    alt="img4" />
                </MDBCol>
                <MDBCol md="2" className="col2-style">
                  <img className="brand-style" src={YAHOO}
                   alt="img2" /> 
                </MDBCol>          
              </MDBRow>
            </MDBCarouselItem>       
        </MDBCarouselInner>
      </MDBCarousel>
      // /* </MDBContainer>   */}
   );
}

export default MultiCarouselPage;