import React, { Component } from 'react';
import './Pannel.css';

export class Pannel extends Component {
    render() {
        return (
             <div className="card text-white bg-primary mb-3" >
                <div className="card-header text-center">Spin Your Luck</div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-lg-3">
                                <p className="pg-style">Offer Title:<span className="bold">Apple iPhone 6s</span>,</p>
                            </div>
                            <div className="col-lg-3">
                                <p className="pg-style">Current Players:<span className="bold">3</span></p>
                            </div>
                            <div className="col-lg-3">
                            <p className="pg-style">Offers Remaining: <span className="bold">1</span></p>
                            </div>
                            <div className="col-lg-3">
                            <p className="pg-style">Merchant Info: <span className="bold">Recibilion</span></p>  
                            </div>
                        </div>
                        <div className="row ">
                            <div className="col-lg-12 game-mode">
                                <p className="pg-style ">Game Mode: <span className="bold">Medium</span></p>  
                            </div>  
                        </div>
                </div>
            </div>
        );
    }
}

export default Pannel;
