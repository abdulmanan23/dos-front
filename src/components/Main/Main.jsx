import React, { Component } from "react";
import Header from "../Layout/Header/Header";
import { Card, Button } from "react-bootstrap";
import { connect } from "react-redux";
import Carousel from "../UI/Carousel/Carousel";
import MultiCarousel from "../UI/MultipleCarousel/MultiCarousel";
import "./Main.css";
import Categories from "../Categories/Categories";
import OnlineMall from "../Online_Mall/OnlineMall";
import Footer from "../Layout/Footer/Footer";
import { toast, Zoom } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { fetchOffersRequest } from "../../services/dataService";
import { fetchOffers, fetchDetails } from "../../redux/actions";
import { NETWORK_ERROR } from "../../constants/Constants";
import ReactTooltip from "react-tooltip";
import { Link } from "react-router-dom";
import moment from "moment";
class Main extends Component {
  async componentWillMount() {
    let offerResponse = await fetchOffersRequest();
    if (offerResponse === NETWORK_ERROR) {
      // toast.error(NETWORK_ERROR);
    } else {
      this.props.fetchOffers(offerResponse);
    }
  }
  detailHandler = (offer) => {
    this.props.fetchDetails(offer);
    this.props.history.push("details");
  };
  render() {
    toast.configure({
      position: toast.POSITION.TOP_CENTER,
      transition: Zoom,
      autoClose: 3000,
    });
    const offeritems = this.props.offers.map((offer) => {
     const date= moment(offer.added_date).format('MMMM Do YYYY');
     console.log("date=",date);
      return (
        <Card
          className="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12 card1-style"
          key={offer.offerId}
        >
          <div className="text-center">
            <Link to="/details">
              <img
                src={offer.thumb}
                alt="pic1"
                className="img-style"
                data-tip
                data-for="imgTip"
                onClick={() => this.detailHandler(offer)}
              />
            </Link>
          </div>
          <Card.Body>
            <Card.Title className="offer-title max-lines">
              {offer.title}
            </Card.Title>
            <Card.Subtitle className="offer-author mt-2 max-lines2">
              Provided By {offer.added_by}
            </Card.Subtitle>
            <Card.Subtitle className="offer-date mt-2"  data-tip  data-for="dateTip">
              {date}
            </Card.Subtitle>
            <Card.Text className="offer-description"></Card.Text>
            {/* <Link to="/details"> */}
            <Button
              className="btn btn-warning"
              data-tip
              data-for="detailsTip"
              onClick={() => this.detailHandler(offer)}
            >
              Get Details
            </Button>
            <ReactTooltip id="imgTip" place="top" effect="solid">
              Click on Image to get more details
            </ReactTooltip>
            <ReactTooltip id="dateTip" place="top" effect="solid">
              Product Registered Date
            </ReactTooltip>

            {/* </Link> */}
          </Card.Body>
        </Card>
      );
    });
    return (
      <div>
        <Header />
        <Carousel />
        <h4 className="first-style">Explore Deals By Department</h4>
        <h6 className="h6-style">Todays Deals</h6>
        <MultiCarousel />
        <h4 className="second-style">Get Exciting Free offers</h4>
        <h6 className="h6-style">Click Details for futher information</h6>
        <div className="container-fluid" style={{ marginTop: "-100px" }}>
          <div className="row row-style">{offeritems}</div>
        </div>
        <Categories />
        <OnlineMall />
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    offers: state.availableOffers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOffers: (offerData) => dispatch(fetchOffers(offerData)),
    fetchDetails: (Details) => dispatch(fetchDetails(Details)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
