import React from "react";
import Pannel from "../UI/Pannel/Pannel";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";

import "./GameScreen.css";
import Footer from "../Layout/Footer/Footer";
import Game from "../Game/Game";
const GameScreen = () => {
  return (
    <div className="game-bg">
      <Pannel />
      <div className="float-right">
        <Link to="/">
          <Button color="info" className="mr-3">
            Quit Game
          </Button>
        </Link>
      </div>

      <div>
        <h4
          style={{ color: "black", marginBottom: "72px", marginTop: "-2px" }}
          className="text-center"
        >
          Try Your Luck
        </h4>
        <Game />
        <Link to="/checkout">
          {/* <button className="btn btn-warning"> Get Code</button> */}
        </Link>
      </div>
      <Footer />
    </div>
  );
};

export default GameScreen;
