import React, { Component } from 'react';
import { Form, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Header from '../Layout/Header/Header';
import Footer from '../Layout/Footer/Footer';
import './CheckOut.css';
class CheckOut extends Component {
    onChange = (e) => {
        if (e.target.value.match("^[a-zA-Z ]*$") != null) {
            this.setState({ [e.target.name]: e.target.value });
        }
    };
    submitHandler = (event) => {
        event.preventDefault();
    }
    render() {
        return (
            <div>
                <Header />
                <div className="container-fluid cont-style">
                    <div className="row">
                        <div className="col-lg-8 first-col ">
                            <h4 className="title-padding">Delivery Information</h4>
                            <Form noValidate onSubmit={this.submitHandler} >
                                <Form.Row>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom01">

                                        <Form.Label >Full Name:</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="email"
                                            placeholder="Enter your first and last name"
                                            onChange={this.onChange}
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom01" className="second-col">
                                        <Form.Label>Province</Form.Label>
                                        <Form.Control as="select" placeholder="Please select">
                                            <option>Punjab</option>
                                            <option>Sindh</option>
                                            <option>Balochistan</option>
                                            <option>Khyber Pakhtunkhwa</option>
                                        </Form.Control>

                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02">
                                        <Form.Label>Phone Number:</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="phone"
                                            placeholder=" Enter your phone number"
                                            onChange={this.onChange}
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02" className="second-col">
                                        <Form.Label >City</Form.Label>
                                        <Form.Control as="select" placeholder="please select your city">
                                            <option>Lahore</option>
                                            <option>Quetta</option>
                                            <option>Azaad Kashmir</option>
                                            <option>Peshawar</option>
                                            <option>Taxilla</option>
                                            <option>Faislabad</option>
                                            <option>Gujrawala</option>
                                            <option>Islamabad</option>
                                        </Form.Control>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02">
                                    </Form.Group>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02" className="second-col">
                                        <Form.Label>Area</Form.Label>
                                        <Form.Control as="select" placeholder="please select your area">
                                            <option>Sadar Cantt</option>
                                            <option>Joher Town</option>
                                            <option>Samnabad</option>
                                            <option>Muslim Town</option>
                                            <option>Iqbal Town</option>
                                            <option>Anarkali</option>
                                            <option>Mall Road</option>
                                            <option>Garden Town</option>
                                        </Form.Control>

                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02">
                                    </Form.Group>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02" className="second-col">
                                        <Form.Label>Address</Form.Label>
                                        <Form.Control  type="text"
                                            name="address"
                                            placeholder="Enter your address"
                                            onChange={this.onChange}>
                                        </Form.Control>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02">
                                    </Form.Group>
                                    <Form.Group as={Col} md="4" xs="7" controlId="validationCustom02" className="second-col">
                                    <Button className="btn btn-info save-btn"  >Save</Button>
                                    </Form.Group>
                                </Form.Row>
                            </Form> 
                        </div>
                        <div className="col-lg-4 ">
                            <h4 className="title2-padding"  >Order Summary</h4>
                            <Form>
                                <div className="col-lg-12 d-flex">
                                    <div className="col-lg-8">
                                        <p className="summary-style">Subtotal(1 items)</p>
                                        <p className="summary-style">Shipping Fee</p>
                                        <p className="summary-style">Shipping Fee Discount</p>
                                        <Form.Row>
                                            <Form.Group as={Col} md="8" xs="12" controlId="validationCustom01">
                                                <Form.Control

                                                    required
                                                    type="text"
                                                    name="email"
                                                    placeholder="Enter voucher code"
                                                    className="input2-padding"
                                                    onChange={this.onChange}
                                                />
                                            </Form.Group>
                                        </Form.Row>
                                    </div>
                                    <div className="col-lg-4 ml-auto">
                                        <p className="price-style">RS 1149</p>
                                        <p className="price-style">RS 130</p>
                                        <p className="price-style">RS 130</p>
                                        <Link to="/">
                                            <Button className="btn btn-info apply-btn" >Apply</Button>
                                        </Link>    
                                    </div>
                                </div>
                            </Form>
                        </div>
                    </div>

                </div>
                <Footer />
            </div>

        );
    }
}

export default CheckOut;
