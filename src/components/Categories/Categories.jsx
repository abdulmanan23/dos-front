import React from 'react';
import { Card } from 'react-bootstrap';
import infant from '../../assets/categories/infant.jpg';
import Perfumes from '../../assets/categories/Perfumes.jpg';
import cables from '../../assets/categories/cables.jpg';
import Body_soaps from '../../assets/categories/Body_soaps.jpg';
import cooling_pads from '../../assets/categories/cooling_pads.jpg';
import Diapers from '../../assets/categories/Diapers.jpg'; 
import Hammers from '../../assets/categories/Hammers.jpg';
import Powdered_drink from '../../assets/categories/Powdered_drink.jpg';
import Tea from '../../assets/categories/Tea.jpg';
import glues from '../../assets/categories/glues.jpg';
import phone_cases from '../../assets/categories/phone_cases.jpg';
import Inks_paints from '../../assets/categories/Inks_paints.jpg';
import footwear from '../../assets/categories/footwear.jpg';
import wall_stickers from '../../assets/categories/wall_stickers.jpg';
import camera from '../../assets/categories/camera.png';
import gamepad from '../../assets/categories/gamepad.png';
import './Categories.css';

import './Categories.css';
const Categories = () => {
    return (
        <div>
            <h4 style={{color:'black'}}>Categories</h4>
            <div className="flex-container">
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={infant} 
                                    alt="pic1"
                                     />
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Infant(0-6 Months)</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Body_soaps} alt="infant"/>
                            </div>
                                <Card.Text className=" padding2" >
                                    <span className="category-title">Body Soaps</span>
                                </Card.Text>
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={cables} alt="infant"/>
                            </div>
                                <Card.Text className=" padding2" >
                                    <span className="category-title">Cables</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={cooling_pads} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">cooling_pads</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Diapers} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Baby Diapers</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Powdered_drink} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Powered Drinks</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Hammers} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Home Tools</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Tea} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Tea</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={cooling_pads} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">cooling_pads</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Inks_paints}
                                    alt="pic1"
                                     />
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Artistic Inks</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={glues} alt="infant"/>
                            </div>
                                <Card.Text className="padding2">
                                    <span className="category-title">Adhesive and Glues</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Perfumes} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Perfumes</span>
                                </Card.Text >
                        </Card>
                </div>
            </div>
            <div className="flex-container">
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Inks_paints}
                                    alt="pic1"
                                     />
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Artistic Inks</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={glues} alt="infant"/>
                            </div>
                                <Card.Text className="padding2">
                                    <span className="category-title">Adhesive and Glues</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Perfumes} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Perfumes</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={phone_cases} alt="infant"/>
                            </div>
                                <Card.Text className="padding2">
                                    <span className="category-title">Phone Cases</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={footwear} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Foot wear</span>
                                </Card.Text>
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={wall_stickers} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Wall Stickers and Deals</span>
                                </Card.Text>
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={camera} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Camera</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={gamepad} alt="infant"/>
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Gamepad</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={glues} alt="infant"/>
                            </div>
                                <Card.Text className="padding2">
                                    <span className="category-title">Adhesive and Glues</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={infant} 
                                    alt="pic1"
                                     />
                            </div>
                                <Card.Text className="padding2" >
                                    <span className="category-title">Infant(0-6 Months)</span>
                                </Card.Text >
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={Body_soaps} alt="infant"/>
                            </div>
                                <Card.Text className=" padding2" >
                                    <span className="category-title">Body Soaps</span>
                                </Card.Text>
                        </Card>
                </div>
                <div>
                    <Card className="card2-style">
                            <div className="text-center">
                                <img src={cables} alt="infant"/>
                            </div>
                                <Card.Text className=" padding2" >
                                    <span className="category-title">Cables</span>
                                </Card.Text >
                        </Card>
                </div>
            </div>
        </div>
    );
}

export default Categories;